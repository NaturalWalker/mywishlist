# MyWishList 2020

# Les liens important du projet

Lien direct vers le site: 

Lien vers les fonctionnalités et divers infos sur le projet: [Lien vers le Excel du site](https://docs.google.com/spreadsheets/d/1AeR2Zm_kNs7_j9U9hQW8RrnLw6dbpnZCpFUPGgSsXxk/edit?usp=sharing)
Un tutoriel est disponible [ici](https://www.youtube.com/watch?v=MXOb6KB1md8).


# Installation

Premierement, telechargez ou clonez le projet dans votre serveur.

**Base de Données**

Une fois l'étape précédente faite, vous trouverez un fichier nommé: *mywishlist.sql*

Ouvrez votre système de gestion de BDD, puis créez une nouvelle BDD nommée: *mywishlist*

Une fois fait, rendez vous dans l'onglet d'importation de BDD, ici vous choisirez le fichier *mywishlist.sql* vu precedement.


**Créer la liaison entre votre site et la BDD**

Dans le dossier du projet, vous trouverez un fichier nommé src/*conf*, dans se fichier créer un document nommé: *conf.ini*.
Dans ce dernier, vous copierez ceci:
* driver =  system de gestion de BDD  (ex: mysql)
* username = username de votre system de gestion de BDD (ex: root)
* password = password de votre system de gestion de BDD (ex: )
* host = Adresse serveur (ex: localhost) 
* database = mywishlist
* charset = utf8




Une fois cela fait, la connexion entre la BDD et le site devrait etre etablie.

**Membres du groupe**
* Erwin MOREL
* florian PELLETIER
* matthieu ADAM
* hugo PISANO
* thibaut ROUSCHMEYER
