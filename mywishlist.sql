-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  Dim 12 jan. 2020 à 15:43
-- Version du serveur :  10.4.6-MariaDB
-- Version de PHP :  7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mywishlist`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `idClient` varchar(10) NOT NULL,
  `nomClient` varchar(40) NOT NULL,
  `prenomClient` varchar(40) NOT NULL,
  `loginClient` varchar(40) NOT NULL,
  `mdpClient` varchar(60) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`idClient`, `nomClient`, `prenomClient`, `loginClient`, `mdpClient`, `admin`) VALUES
('3fe8083732', '', '', '', '$2y$10$B.yQPwZhCYZdTURBI3g.iOiL2bAtlganCgQhvF71lWjhqrDfcBKiO', 0),
('741c5070af', 'new nom', 'new', 'demonstration', '$2y$10$VxmCeGPWfP8nMmb0UXByp.9DmV31C/Em/cwAAnke.P0xR8HLBC.UK', 0),
('e3cbf8b260', 'Olivier', 'CarGlass', 'XxOlivierxX', '$2y$10$FMou473rLCYvXjW3Bt8gEOJdHHO1cmavIO3wFPByAXcEv5AufZ/Sm', 0),
('S10i1U79Mb', 'Natural', 'Walker', 'NaturalWalker', '$2y$10$9D/gIPyf9XxcDwB5/1vWdej.Pj1bBvYVnTW0QZ4MIl6Ul0Xbwckr6', 0);

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `liste_id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `descr` text DEFAULT NULL,
  `img` text DEFAULT NULL,
  `estReserver` tinyint(1) NOT NULL DEFAULT 0,
  `tarif` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `item`
--

INSERT INTO `item` (`id`, `liste_id`, `nom`, `descr`, `img`, `estReserver`, `tarif`) VALUES
(1, 2, 'Champagne', 'Bouteille de champagne + flutes + jeux à gratter', 'champagne.jpg', 0, '20.00'),
(2, 2, 'Musique', 'Partitions de piano à 4 mains', 'musique.jpg', 0, '25.00'),
(3, 2, 'Exposition', 'Visite guidée de l’exposition ‘REGARDER’ à la galerie Poirel', 'poirelregarder.jpg', 0, '14.00'),
(4, 3, 'Goûter', 'Goûter au FIFNL', 'gouter.jpg', 0, '20.00'),
(5, 3, 'Projection', 'Projection courts-métrages au FIFNL', 'film.jpg', 0, '10.00'),
(6, 2, 'Bouquet', 'Bouquet de roses et Mots de Marion Renaud', 'rose.jpg', 0, '16.00'),
(7, 2, 'Diner Stanislas', 'Diner à La Table du Bon Roi Stanislas (Apéritif /Entrée / Plat / Vin / Dessert / Café / Digestif)', 'bonroi.jpg', 0, '60.00'),
(8, 3, 'Origami', 'Baguettes magiques en Origami en buvant un thé', 'origami.jpg', 0, '12.00'),
(9, 3, 'Livres', 'Livre bricolage avec petits-enfants + Roman', 'bricolage.jpg', 0, '24.00'),
(10, 2, 'Diner  Grand Rue ', 'Diner au Grand’Ru(e) (Apéritif / Entrée / Plat / Vin / Dessert / Café)', 'grandrue.jpg', 0, '59.00'),
(11, 0, 'Visite guidée', 'Visite guidée personnalisée de Saint-Epvre jusqu’à Stanislas', 'place.jpg', 0, '11.00'),
(12, 2, 'Bijoux', 'Bijoux de manteau + Sous-verre pochette de disque + Lait après-soleil', 'bijoux.jpg', 0, '29.00'),
(19, 0, 'Jeu contacts', 'Jeu pour échange de contacts', 'contact.png', 0, '5.00'),
(22, 0, 'Concert', 'Un concert à Nancy', 'concert.jpg', 0, '17.00'),
(23, 1, 'Appart Hotel', 'Appart’hôtel Coeur de Ville, en plein centre-ville', 'apparthotel.jpg', 0, '56.00'),
(24, 2, 'Hôtel d\'Haussonville', 'Hôtel d\'Haussonville, au coeur de la Vieille ville à deux pas de la place Stanislas', 'hotel_haussonville_logo.jpg', 0, '169.00'),
(25, 1, 'Boite de nuit', 'Discothèque, Boîte tendance avec des soirées à thème & DJ invités', 'boitedenuit.jpg', 0, '32.00'),
(26, 1, 'Planètes Laser', 'Laser game : Gilet électronique et pistolet laser comme matériel, vous voilà équipé.', 'laser.jpg', 0, '15.00'),
(27, 1, 'Fort Aventure', 'Découvrez Fort Aventure à Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut à l\'élastique inversé, Toboggan géant... et bien plus encore.', 'fort.jpg', 0, '25.00'),
(44, 4, 'avatar', 'im designing avanaezar', 'my-avatar.jpg', 0, '5.00'),
(47, 17, 'fdfd', 'fdfdfd', 'Capture.PNG', 0, '50.00'),
(48, 70, 'bretagne', 'photo', 'breta.jpg', 0, '200.00'),
(49, 102, 'Avatar', 'ListePublique', 'my-avatar.jpg', 0, '2.00'),
(50, 116, 'fabrice', 'zdaza', 'hh.PNG', 0, '666.00'),
(51, 116, 'zdzdz', 'dzdzdz', 'kk.png', 0, '2.00'),
(52, 119, 'WWOualia', 'nzhenfkjehznfujkzef', 'kk.jpg', 0, '23.00'),
(54, 118, 'Jean Michel', 'Achat de jus de banane', 'téléchargement.gif', 1, '6.50'),
(56, 122, 'Twitter logo', 'test descr', 'twitter.png', 1, '2.50'),
(57, 123, 'Breaking Bad', 'I am the one who knocks', 'i-am-the-one-who-knocks-breaking-bad-wallpaper-5109.jpg', 1, '6.00'),
(58, 124, 'New Name', 'new desc', 'téléchargement.png', 1, '3.00');

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

CREATE TABLE `liste` (
  `no` int(11) NOT NULL,
  `user_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiration` date DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `liste`
--

INSERT INTO `liste` (`no`, `user_id`, `titre`, `description`, `expiration`, `token`, `public`) VALUES
(118, NULL, 'huzahdaz', 'henuifhaiuzefhaz', '3333-03-31', 'rwW3TMVQ5qMc5xpodkziJa1teqyCN0P2hHXP7ZmTKY87ngMR76rDu95', 1),
(120, 'S10i1U79Mb', 'zzzzzzz', 'zzzzzzzzzz', '2020-11-21', 'AmHsv85K8Wx98V3XT3nYyc5ZOdR23UVny3lPl2XjGL8OZS3tvwPkGSy', 1),
(122, 'e3cbf8b260', 'Liste Olivier', 'ma liste', '2019-04-04', 'ee6e1785ff3a5e09784d126b99f69190233bedee75cf00173ad7986e474052ed3b42535631254c8ab8039ff8d360197129116b68ca87e6', 0),
(123, 'e3cbf8b260', 'ListeOlivier date valide', 'mmmm', '2222-02-20', '78f087f924a7efa4818fcbaec193295b727919ef9bdb2f3c44e7ca2ad588afc3d838206940628aeaaf0eb10988f2ec9fdf3fcd3264cec2', 0);

-- --------------------------------------------------------

--
-- Structure de la table `messageslistes`
--

CREATE TABLE `messageslistes` (
  `idMessage` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `idListe` int(11) NOT NULL,
  `nomC` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `messageslistes`
--

INSERT INTO `messageslistes` (`idMessage`, `message`, `idListe`, `nomC`) VALUES
(1, 'testMessage', 120, 'naturalwalker'),
(2, 'Bonjour bel item', 120, 'Police'),
(3, 'Vous étes un super créateur', 118, 'Naturalwalker'),
(4, 'Bonjour bienvenue sur ma liste', 122, 'XxolivierxX'),
(5, 'Vous etes un tres bon createur', 118, 'Thibaut R'),
(6, 'Bonjour a tous', 124, 'demonstration');

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `idReservation` varchar(25) NOT NULL,
  `idItem` int(11) NOT NULL,
  `nomParticipant` varchar(40) NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`idReservation`, `idItem`, `nomParticipant`, `message`) VALUES
('09c131d2d6145d44a5fb18d5b', 56, 'Michel Bernard', 'Beau produit '),
('0b3c32eaee00ac635f6815b79', 58, 'demonstration', 'yes'),
('0bb815d19428a5cfbc22c5ab3', 55, 'naturalwalker', ''),
('76a258a65810ea97400742a76', 56, 'XxolivierxX', 'Vraiment content'),
('858848709ebad58b8dac48a66', 54, 'Thibaut R', 'Il a l&#39;air vraiment bien'),
('8f63543a43c2762012276544a', 54, 'JM Lepain', 'Merci de me donner cet objet svp'),
('d71a59460b0c87aca7b2d7cd7', 57, 'xxolivierxx', 'Beau poster');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idClient`) USING BTREE,
  ADD KEY `loginClient` (`loginClient`);

--
-- Index pour la table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `liste`
--
ALTER TABLE `liste`
  ADD PRIMARY KEY (`no`);

--
-- Index pour la table `messageslistes`
--
ALTER TABLE `messageslistes`
  ADD PRIMARY KEY (`idMessage`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`idReservation`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT pour la table `liste`
--
ALTER TABLE `liste`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT pour la table `messageslistes`
--
ALTER TABLE `messageslistes`
  MODIFY `idMessage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
