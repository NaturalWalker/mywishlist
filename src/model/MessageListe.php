<?php

namespace mywishlist\model;

use \Illuminate\Database\Eloquent\Model;

class MessageListe
    extends Model
    {
        protected $table = 'messagesListes';
        protected $primaryKey = 'idMessage';
        public $timestamps = false;
      
        public function message() {return $this->belongsTo('mywishlist\model\Listes','idListe','no');}
    }