<?php

namespace mywishlist\outils;
use Slim\App;
class CreateSingleton
{

    private static $instance;
    private $routeur;
    private $app;
    private $path;
    private $directory;

    private function __contruct(){
        $this->routeur = $this->slim->getContainer()->get('router');
        $config = ['settings' => ['displayErrorDetails' => true]];
        $this->app = new App($config);
        $this->path = '/';
        $this->directory = '/';
    }




    public static function getInstance()
    {
        if (!isset(self::$instance)) 
            self::$instance = new CreateSingleton(); 
        return self::$instance;
    }


    /* getter et setter */
    public function getApp(){ return $this->app; }
    public function setPath($setPath){ $this->path = $setPath; }
    public function setDirectory($setDirectory){ $this->directory = $setDirectory; }
    public function getRouteur(){ return $this->routeur; }
    public function getPath(){ return $this->Path; }
    public function getBaseDir(){ return $this->directory; }
}

