<?php

namespace mywishlist\view;
require_once 'src/vendor/autoload.php';
use mywishlist\view\PrincipalView;
use mywishlist\controller\AffichageController;
use mywishlist\controller\CreationController;
class HomeView
{

    function render(){
        $app = PrincipalView::render();
        return $app;
    }

    function renderListePublique(){
        $app = AffichageController::displayLiPub();
        return $app;
    }

    function renderListeCreation(){
        $app = AffichageController::displayFormCreaListe();
        return $app;
    }

    function renderCreaListe(){
        $app = CreationController::creationListe();
        return $app;
    }

    function renderListeT($setToken){
        $app = AffichageController::displayListeToken($setToken);
    }

    function renderAllItem(){
        $app = AffichageController::displayAllItem();
    }

    function renderCreaItem($setToken){
        $app = AffichageController::displayFormCreaItem($setToken);
    }
    function renderCreaItems($setToken){
        $app = CreationController::creationItem($setToken);
    }

    function renderAccountForm(){
        $app = AffichageController::displayFormCreaCompte();
    }
    function renderCreaCompte(){
        $app = CreationController::creationCompte();
    }
    
    function renderConnexionForm(){
        $app = AffichageController::displayFormConnexion();
    }
    function renderConnexion(){
        $app = CreationController::connexionCompte();
    }

    function renderMeslistess(){
        $app = AffichageController::displayMesListess();
    }

    function renderDecoCompte(){
        $app =CreationController::deconnectionCompte();
    }
}