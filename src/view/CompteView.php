<?php

namespace mywishlist\view;
require_once 'src/vendor/autoload.php';
use mywishlist\controller\SessionController;
class CompteView
    extends GeneralView
    {
        function __construct() { parent::__construct();}

        public  function renderListeForm() {
    
     

            $content =  "<main>
 
            <form method='post' >
                    <div id='connexion-logo'>
                            <img src='/mywishlist/img/logo.png'>
                    </div>     
                <div>
                    Nom : <input type='text' name='nom' placeholder='Entrez votre nom' />
                </div>
                <div>
                    Prenom : <input type='text' name='prenom' placeholder='Entrez votre prenom' />
                </div>
                <div>
                Pseudo : <input type='text' name='pseudo' placeholder='Entrez votre Pseudo' />
                </div>
                <div>
                    Mot de Passe : <input type='password' name='mdp' placeholder='Entrez votre mdp' />
                </div>
                <div>
                        Mot de Passe : <input type='password' name='mdpverif' placeholder='Entrez votre mdp' />
                </div>
                <div>
                <input type='submit' value='Creation du Compte' />
                </div>
            </form>
            </main>";
      
         
            $content = str_replace("\n", "\n  ", $content);
            $this->addContent($content);
            parent::render();
          }


          public function renderCompteConnexion(){
              $content = "<main>
 
              <form  method='post'>
                  <div id='connexion-logo'>
                          <img src='/mywishlist/img/logo.png'>
                  </div>     
                  <div>
                      Login : <input type='text' name='login' placeholder='Entrer votre pseudo' />
                  </div>
                  <div>
                      Mot de Passe : <input type='password' name='mdp' placeholder='Entrez votre mdp' />
                  </div>
                  <div>
                  <input type='submit' value='Connexion' />
                  </div>
              </form>
            </main>";

            $content = str_replace("\n", "\n  ", $content);
            $this->addContent($content);
            parent::render();
          }

        public function renderCompteOptions(){
            $content = "<main><form>";
            if(!SessionController::estCon()){
           $content.=" <button style='height:35px;width:255px;'><a href='/mywishlist/index.php/compte/creerCompte'>Créer son Compte</a></button>";
           $content.=" <button style='height:35px;width:255px;'><a href='/mywishlist/index.php/compte/connexion'>Se Connecter</a></button> ";
            }
            else
            {
           
             $content.="  <button style='height:35px;width:255px;'><a href='/mywishlist/index.php/list/meslistes'>Mes Listes</a></button>";
             $content.="  <button style='height:35px;width:255px;'><a href='/mywishlist/index.php/compte/deconnexion'>Se Deconnecter</a></button>";
             $content.="  <button style='height:35px;width:255px;'><a href='/mywishlist/index.php/compte/modifier'>Modifier Mon Compte</a></button>";
             $content.="  <button style='height:35px;width:255px;'><a href='/mywishlist/index.php/compte/supprimer'>Supprimer Mon Compte</a></button>";
            }            
            
           $content.="</form>
          </main>";

          $content = str_replace("\n", "\n  ", $content);
          $this->addContent($content);
          parent::render();
        }

        public function renderListeCreateurs($lisCreateur){
            $content = "<main><section>";
            $content.= "<div class='un'>";
            $content.="<h2>Listes Des Createurs</h2>";
            
            foreach($lisCreateur as $crea){
               
                $content .= "<h3>nom: $crea->nomClient, prenom: $crea->prenomClient | pseudo: $crea->loginClient</h3>";
                
            }    
            $content .="</div></section></main>";

            $content = str_replace("\n", "\n  ", $content);
            $this->addContent($content);
            parent::render();
        }

        public function renderModifCompte(){
            $content =  "<main>
 
            <form method='post' >
                    <div id='connexion-logo'>
                            <img src='/mywishlist/img/logo.png'>
                    </div>     
                <div>
                    Nom (optionnel): <input type='text' name='nom' placeholder='Entrez un nouveau nom' />
                </div>
                <div>
                    Prenom (optionnel): <input type='text' name='prenom' placeholder='Entrez un nouveau prenom' />
                </div>
                <div>
                    Mot de Passe (optionnel) : <input type='password' name='mdp' placeholder='Entrez un nouveau mdp' />
                </div>
                <div>
                        Mot de Passe (optionnel): <input type='password' name='mdpverif' placeholder='Entrez un nouveau mdp' />
                </div>
                <div>
                <input type='submit' value='Modifier mon Compte' />
                </div>
            </form>
            </main>";
      

          $content = str_replace("\n", "\n  ", $content);
          $this->addContent($content);
          parent::render();
        }




    }