<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="./img/favicon.ico" type="image/x-icon"><link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/mywishlist/css/styles.css">
    <title>MyWishList - 2020</title>
    <nav>
        <div id="logo-img">
            <a href="/mywishlist/">
                <img src="/mywishlist/img/logo.png" alt="logo">
            </a>
        </div>
        <div id="menu-icon">
        </div>
        <ul>
            <li>
                <a href="/mywishlist/">Home</a>
            </li>
            <li>
                <a href="/mywishlist/index.php/listes">Listes</a>
            </li>
            <li>
                <a href="/mywishlist/index.php/items">Items</a>
            </li> 
            <li>
                <a href="/mywishlist/index.php/createur">Createurs</a>
            </li> 
            
            <li>
                <a href="/mywishlist/index.php/compte">MonCompte & MesListes</a>
            </li>
            
        </ul>
    </nav>
    
    </head>