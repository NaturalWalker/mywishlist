<?php
namespace mywishlist\view;

  class PrincipalView extends GeneralView {

    function __construct() {
      parent::__construct();
    }

    /**
     *Génère le contenu HTML pour afficher la page d'accueil
     */
    function render() {
        $content = " <div id='TestBanner'></div>";
        $content .= "<h1>Bienvenue sur MyWishList</h1>\n";
        $content .=
"<p>
    Vous souhaitez créer une liste de souhaits pour un anniversaire ou un mariage ?
    Ou tout événement vous tenant à coeur ?
    Vous vous trouvez au bon endroit !
</p>
<p>
Хотите создать список пожеланий на день рождения или свадьбу ? Или любое событие, которое вас волнует ? Вы оказались в нужном месте !
</p>
<p>
هل تريد إنشاء قائمة أمنيات لعيد الميلاد أو الزفاف ؟ أو أي شيء قريب من قلبك ؟ أنت في المكان الصحيح !
</p>

"
;
        parent::addContent($content);
        parent::render();
    }
 }
