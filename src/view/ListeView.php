<?php

namespace mywishlist\view;
require_once 'src/vendor/autoload.php';
use mywishlist\model\Listes;
use mywishlist\model\Items;
use mywishlist\controller\AffichageController;
use mywishlist\controller\CreationController;
use mywishlist\controller\SessionController;
use Slim\App;
class ListeView 
    extends GeneralView
{
    function __construct() { parent::__construct();}

    /* Pour l'instant je ne traite que les liste publique donc proprio sera toujours a false*/
    function renderLpub($liste, $proprio)
    {

        $content = "<main><section>";


        if(count($liste)==0){ $content.="<h2>Aucune liste publique trouvee</h2>";}
        else{
            $content.= "<div class='un'>";
            $content.="<h2>Liste des wishlist publique</h2><ul>";
            foreach($liste as $l)
            {
                $content.= "<div class='BannerMC'>";
                if(strtotime($l->expiration)-time()>0)
                {
                    $date = ucwords(strftime('%d %B %Y', strtotime($l->expiration)));
                    $url = 
                    $content .= "    <li> <a href='/mywishlist/index.php/listes/$l->token'> $l->titre </a><a>|</a> <a>$l->description</a><a>|</a><span class=\"listeDateExpiration\"> Expire le : $date  </span> </li>\n";
                 
                }
                $content.= "</div>";
            }
            $content.= "</div>";
        }
        $content .= "<button><a href='/mywishlist/index.php/listes/creation'>Creer Ma Liste</a></button></section></main>";
        $content = str_replace("\n", "\n  ", $content);
        $this->addContent($content);
        parent::render();
    }



    function renderListeForm()
    {
        $tableau="<main>
 
        <form  method='POST'>
            <div id='connexion-logo'>
                    <img src='/mywishlist/img/logo.png'>
            </div>     
            <div>
                Titre : <input type='text' name='titre' placeholder='Entrer le titre' />
            </div>
            <div>
                description : <input type='text' name='description' placeholder='Entrez la description' />
            </div>
            <div>
            description : <input type='date' name='dateExpiration' placeholder='Entrez une date d expiration' />
        </div>
            <div>
            <input type='submit' value='creation de la liste' />
            </div>
        </form>
</main>";

    $tableau = str_replace("\n", "\n  ", $tableau)."\n";
    $this->addContent($tableau);
    parent::render();
    }


    function renderListeToken($listeToken){
        if($listeToken){
            $isPP = 'Private';
            $content='';
            $itm='';
            if($listeToken->public == 1){
                $isPP = 'Public';
                $content.="<form style='margin-top:200px;'><button style='height:35px;width:255px;'><a href='/mywishlist/index.php/listes/$listeToken->token/message'>Envoyer un Message</a></button></form>";
            }
            $content .= "<main><section><div><h2> $listeToken->titre </h2>";
            $content .= "<p> $listeToken->description </p>";
            $content .= "<h3>La liste est: $isPP</h3>";
            $content .= "</br><p>Cette liste est composee des items suivants:</p></br><ul>";
            if($listeToken){
            $content.= "<div class='un'>";
      
            foreach($listeToken->items as $itm){
            $content .= "<h2><a href='/mywishlist/index.php/items/$itm->id'>$itm->nom</a></h2>";
            $content .= "<img src='/mywishlist/img/$itm->img' style='width:100px'>";
            $content .= "<h3>$itm->descr</h3>";
            $content .= "<h3>$itm->tarif</h3>";
           
            }
            $content.= "</div>";
        }
            $content.= "</section><div class='un'>
            <h2>Les Messages :</h2>";
            foreach($listeToken->messages as $mess){
                $content .= "<h3>$mess->nomC : $mess->message</h3>";
            }
            $content.= "</div>";
            if($itm){
            $content.= "</section><div class='un'>
            <h2>Les Items reservés :</h2>";
           
            foreach($itm->reservation as $res){

                $content .= "<h3> $res->nomParticipant : $res->message | A reservé: $itm->nom</h3>";
            }
            $content.= "</div>";
             }
            $idlis = $listeToken->no;
            $content.= "</ul></div></section></main>";
            if(SessionController::estCon()){
                if(strtotime($listeToken->expiration)-time()>0)
                {
               $content .=" <form><button style='height:35px;width:255px;'><a href='/mywishlist/index.php/listes/$idlis/publicprive'>Private/Public</a></button>";
               $content .=" <form><button style='height:35px;width:255px;'><a href='/mywishlist/index.php/listes/$idlis/supprimer'>Supprimer Liste</a></button>";
            $content.="<button style='height:35px;width:255px;'><a href='/mywishlist/index.php/listes/$listeToken->token/modification'>modification de la Liste</a></button>";
            $content.="<button style='height:35px;width:255px;'><a href='/mywishlist/index.php/listes/$listeToken->token/creationItem'>Ajout d'un item</a></button>";
            $content.="</form><br/><br/></div></li>";
                }
            }
        }
        else{
            $content = "<h2>Pas de liste trouvee avec le token</h2>";
        }
       
        $content = str_replace ("\n", "\n  ", $content);
        $this->addContent($content);
        parent::render();
    }





    public function renderMesListes($MesListes){
        $content = "<main><section>";
        if(isset($_SESSION['login']) && $_SESSION['estConnecte'] == true)
        
        {
            
            if($MesListes){
               
                    $content.= "<div class='un'>";
                    $content.="<h2>Mes listes valide</h2><ul>";
                    foreach($MesListes as $l)
                    {
                        $i=0;
                        $content.= "<div class='BannerMC'>";
                        if(strtotime($l->expiration)-time()>0)
                        {
                         
                          $date = ucwords(strftime('%d %B %Y', strtotime($l->expiration)));
                            $param =$l->no;
                            
                            if($l->public == 0){
                                $p = "Private";
                            }
                            else
                            {
                                $p = "Public";
                            }

                            $content .= "<li><a href='/mywishlist/index.php/listes/$l->token'> $l->titre </a><a> | </a> <a>$l->description</a><a> | </a><span class=\"listeDateExpiration\"> Expire le : $date  </span><a> | </a><a>$p</a></li>";
                        }
            
                        $content.= "</div>";
                        $i++;
                    }
                    $content.= "</div></section>";
                

                    $content.= "<div class='un'>";
                    $content.="<h2>Mes listes date échéance dépassée</h2><ul>";
                    foreach($MesListes as $l)
                    {
                        $i=0;
                        $content.= "<div class='BannerMC'>";
                        if(strtotime($l->expiration)-time()<0)
                        {
                         
                          $date = ucwords(strftime('%d %B %Y', strtotime($l->expiration)));
                            $param =$l->no;
                            
                            if($l->public == 0){
                                $p = "Private";
                            }
                            else
                            {
                                $p = "Public";
                            }

                            $content .= "<li><a href='/mywishlist/index.php/listes/$l->token'> $l->titre </a><a> | </a> <a>$l->description</a><a> | </a><span class=\"listeDateExpiration\"> A Expire le : $date  </span><a> | </a><a>$p</a></li>";
                        }
            
                        $content.= "</div>";
                        $i++;
                    }
                    $content.= "</div>";

                $content .= "</section></main>";
             
            }
            else
            {
                $content.="<h2>Aucune liste publique trouvee</h2>";
            }
        }
        else
        {
            $content.="<h2>Veuillez vous connecter pour acceder a cette page</h2>";
        }
        $content = str_replace("\n", "\n  ", $content);
        $this->addContent($content);
        parent::render();
    }

    public function renderModifListe($idtoken){
        $tableau="<main>
 
        <form  method='POST'>
            <div id='connexion-logo'>
                    <img src='/mywishlist/img/logo.png'>
            </div>     
            <div>
                Titre : <input type='text' name='newtitre' placeholder='Entrer le nouveau Titre' />
            </div>
            <div>
                description : <input type='text' name='newdescription' placeholder='Entrez la nouvelle description' />
            </div>
            <div>
            description : <input type='date' name='newdateExpiration' placeholder='Entrez une nouvelle date d expiration' />
        </div>
            <div>
            <input type='submit' value='Modification de la Liste' />
            </div>
        </form>
</main>";

    $tableau = str_replace("\n", "\n  ", $tableau)."\n";
    $this->addContent($tableau);
    parent::render();
    }

    public function renderMessageListe($lis){
        $tableau="<main>
 
        <form  method='POST'>
            <div id='connexion-logo'>
                    <img src='/mywishlist/img/logo.png'>
            </div>     
            <div>
                (si vous n etes pas connecte): <input type='text' name='nomprenom' placeholder='Entrez votre nom et prenom' />
            </div>
            <div>
                Message : <input type='text' name='message' placeholder='Entrez votre message' />
            </div>
            <div>
            <input type='submit' value='Envoyer le message' />
            </div>
        </form>
</main>";

    $tableau = str_replace("\n", "\n  ", $tableau)."\n";
    $this->addContent($tableau);
    parent::render();
    }

}


