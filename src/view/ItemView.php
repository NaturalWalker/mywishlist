<?php

namespace mywishlist\view;
require_once 'src/vendor/autoload.php';
use mywishlist\model\Listes;
use mywishlist\model\Items;
use mywishlist\controller\SessionController;
use mywishlist\controller\CreationController;
class ItemView
    extends GeneralView
    {
        function __construct() { parent::__construct();}

        function renderItemID($setItem){
            if($setItem){
                if($setItem->estReserver == true){
                    $rev = 'Deja Reservé';
                }
                else
                {
                    $rev = 'pas encore reservé';
                }
                $content = "<main><section><div class='un'><h2> $setItem->nom </h2>";
                $content .= "<img src='/mywishlist/img/$setItem->img' style='width:200px;'>"; 
                $content .= "<p> $setItem->descr </p>";
                $content .= "<p> $rev </p>";
                $content .= "<p> $setItem->tarif euros</p>";
                
                if($setItem->estReserver == false){
                $content .=" <button style='height:35px;width:255px;'><a href='/mywishlist/index.php/items/$setItem->id/reserver'>Reserver cet item</a></button>";
               
                }

                if(SessionController::estCon()){
                    
               

                    if($setItem->estReserver == false){
                        if($setItem->img){
                            $content .=" <button style='height:35px;width:255px;'><a href='/mywishlist/index.php/items/$setItem->id/supprimerImage'>Supprimer Image</a></button>";
                            }
                            else
                            {
                                $content .=" <button style='height:35px;width:255px;'><a href='/mywishlist/index.php/items/$setItem->id/ajoutImage'>Ajouter une Image</a></button>";
                            }
                        $content .=" <button style='height:35px;width:255px;'><a href='/mywishlist/index.php/items/$setItem->id/supprimer'>Supprimer cet item</a></button>";
                        $content .=" <button style='height:35px;width:255px;'><a href='/mywishlist/index.php/items/$setItem->id/modifier'>Modifier cet item</a></button>";
                       
                    }
                }
               $content.=" </div></section></main>";
            }
            $content = str_replace ("\n", "\n  ", $content);
            $this->addContent($content);
            parent::render();
        }
        
        function renderAllItems($item){
            $content = "<main><section>";
            if($item){
               
                $content.= "<div class='un'>";
                $content.="<h1>Liste des Items</h1>";
                foreach($item as $itm){
                $content .= "<h2>$itm->nom</h2>";
                $content .= "<img src='/mywishlist/img/$itm->img' style='width:200px'>";
                $content .= "<h3>$itm->descr</h3>";
                $content .= "<h3>$itm->tarif</h3>";
                }
                $content.= "</div>";
            }
            else
            {
                $content = "<h2>Pas d'items</h2>";
            }
            $content .= "</section></main>";
            $content = str_replace("\n", "\n  ", $content);
            $this->addContent($content);
            parent::render();
        }

        function renderItemForm($liste){
            $tableau="<main>
 
        <form  method='POST' enctype='multipart/form-data'>
            <div id='connexion-logo'>
                    <img src='/mywishlist/img/logo.png'>
            </div>     
            <div>
                Nom : <input type='text' name='nomItem' placeholder='Entrer le nom' />
            </div>
            <div>
                description : <input type='text' name='description' placeholder='Entrez la description' />
            </div>
            <div>
                 Tarif : <input type='text' name='tarifItem' placeholder='Entrez un tarif' />
            </div>
            <div>
                    Image : <input type='file' name='itemImg'>
            </div>
            <div>
            <input type='submit' value='creation de la liste' />
            </div>
        </form>
</main>";

    $tableau = str_replace("\n", "\n  ", $tableau)."\n";
    $this->addContent($tableau);
    parent::render();
        }





        function reserverItem($idItm){
            $tableau="<main>
 
            <form  method='POST' enctype='multipart/form-data'>
                <div id='connexion-logo'>
                        <img src='/mywishlist/img/$idItm->img'>
                </div>     
                <div>
                    Nom et Prenom : <input type='text' name='nomprenom' placeholder='Entrer votre Nom et Prenom' />
                </div>
                <div>
                Message(Optionnel) : <input type='text' name='message' placeholder='Entrer votre Message' />
            </div>
                <div>
                <input type='submit' value='Reserver item' />
                </div>
            </form>
    </main>";
    
        $tableau = str_replace("\n", "\n  ", $tableau)."\n";
        $this->addContent($tableau);
        parent::render();
        }

        public function renderuploadImg($idId){
            $tableau="<main>
 
            <form  method='POST' enctype='multipart/form-data'>
            <div>
            Image : <input type='file' name='itemImg'>
            </div>
            <div>
            <input type='submit' value='Upload de l image' />
            </div>
            </form>
    </main>";
    
        $tableau = str_replace("\n", "\n  ", $tableau)."\n";
        $this->addContent($tableau);
        parent::render();
        }


        public function renderModifItm($itm){
            $tableau="<main>
 
            <form  method='POST' enctype='multipart/form-data'>
                <div id='connexion-logo'>
                        <img src='/mywishlist/img/$itm->img'>
                </div>     
                <div>
                    Nom (optionnel): <input type='text' name='nomItem' placeholder='Entrer le nouveau nom' />
                </div>
                <div>
                    description (optionnel): <input type='text' name='description' placeholder='Entrez la nouvelle description' />
                </div>
                <div>
                     Tarif (optionnel): <input type='text' name='tarifItem' placeholder='Entrez un nouveau tarif' />
                </div>
                <div>
                <input type='submit' value='Modification de la liste' />
                </div>
            </form>
    </main>";
    
        $tableau = str_replace("\n", "\n  ", $tableau)."\n";
        $this->addContent($tableau);
        parent::render();
        }
    }